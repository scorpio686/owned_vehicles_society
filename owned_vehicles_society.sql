-- Structure de la table `owned_vehicles_society`
--

DROP TABLE IF EXISTS `owned_vehicles_society`;
CREATE TABLE IF NOT EXISTS `owned_vehicles_society` (
  `plate` varchar(12) NOT NULL COMMENT 'Plaque de la voiture',
  `vehicle` longtext NOT NULL,
  `society` varchar(60) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Etat de la voiture',
  `reboot` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Si demande fourrière reboot',
  `fourriere` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Voiture à la fourrière',
  `name` varchar(100) DEFAULT NULL COMMENT 'Nom personnalisé'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
